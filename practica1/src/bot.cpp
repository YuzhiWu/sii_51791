//archivo bot.cpp
// yuzhi wu 51791

#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <sys/mman.h>
#include <errno.h>
#include <stdlib.h>
#include "DatosMemCompartida.h"

int main(int argc, char *argv[]){
	const char * fic="/tmp/bot";
	int fd=open(fic,O_RDWR);
	if(fd==-1)
	{
		perror("error_fd:open()");
		exit(EXIT_FAILURE);
	}
	DatosMemCompartida *datos;
	int size=sizeof(DatosMemCompartida);
	datos=static_cast<DatosMemCompartida*>(mmap(NULL,size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0));
	if (datos==MAP_FAILED)
	{
		perror(" Memory Map Failed, QUIT!");
	}
	if(close (fd)==-1) 
	{
		perror("error_ffd:close()");
	}
	while(datos->raq1!=5)
 	{
		if(datos->esfera.centro.x<-0.5f)
			datos->accion_1();
		if(datos->esfera.centro.x<-0.5f)
			if(datos->raq2==2)
				datos->accion_2();
		usleep(25000);
	}

	if(munmap(datos, size)==-1)
	{
		perror("Bad Parent Close, Quit!");
		exit(EXIT_FAILURE);
	}
	 if(unlink(fic)==-1)
	{
		perror("error link()");	
	}
	return 0;
}
	
	
	
