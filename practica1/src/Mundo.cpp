// Yuzhi Wu 51791
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#include <sys/types.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#define TAM 100
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	valor=0;
}

CMundo::~CMundo()
{
	const char *fin_juego="Fin del juego\n";
	if(write (fd,fin_juego,strlen(fin_juego)+1)==-1)
	{
		perror("error_write1: write()");	
	}
	if(close (fd)<0)
	{
		perror("error_mundo:close()");
	}
	datos->raq1=10;
	if(munmap(datos, sizeof(DatosMemCompartida))==-1)
	{
		perror("Bad Parent Close, Quit!");
		exit(EXIT_FAILURE);
	}
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		Esfera e;
	        pelotas.push_back(e);
		char cad[TAM];
		sprintf(cad, "Jugador 2 marca 1 punto, tiene un total de %d\n",puntos2);
		write (fd,cad,strlen(cad)+1);

	}

	if(fondo_dcho.Rebota(esfera))
	{
		
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		Esfera e;
		pelotas.push_back(e);
		char cad[TAM];
		sprintf(cad, "Jugador 1 marca 1 punto, tiene un total de %d\n",puntos1);
		write (fd,cad,strlen(cad)+1);
	}
	
	datos->esfera=esfera;
	datos->raqueta1=jugador1;
	datos->raqueta2=jugador2;      
	if(datos->raq1== 1)OnKeyboardDown('w',0,1);
	if(datos->raq1==-1)OnKeyboardDown('s',0,1);
	if(datos->raq2== 1)OnKeyboardDown('o',0,1);
	if(datos->raq2==-1)OnKeyboardDown('l',0,1);

	if(espera_juego>10.0f)
		datos->raq2=2;
	else
		datos->raq2=0;
	espera_juego=espera_juego+0.025f;
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':datos->raqueta1.velocidad.x=-1;break;
//	case 'd':datos->raqueta1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	if(y!=1) espera_juego=0.0f;
}

void CMundo::Init()
{

	const char * n_fifo = "/tmp/fifo";
	fd=open(n_fifo,O_RDWR);
	espera_juego=0.0f;
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


	int ffd=open("/tmp/bot",O_RDWR| O_CREAT| O_TRUNC, (mode_t) 0666);
	if (ffd < 0)
	{
		perror("Failed mmap file, QUIT!");
	}
	h.esfera=esfera;
	h.raqueta1=jugador1;
	int size=sizeof(DatosMemCompartida);
	if( write(ffd,&h,size)==-1)
	    {
		perror("error write_ffd");
		exit(EXIT_FAILURE);
	    }
	datos=static_cast<DatosMemCompartida*>(mmap(NULL,size,PROT_READ|PROT_WRITE,MAP_SHARED,ffd,0));
	
	if (datos==MAP_FAILED)
	{
		perror(" Memory Map Failed, QUIT!");
	}

	if( close (ffd)==-1)
	{
		perror("error_ffd:close()");
	}


}
