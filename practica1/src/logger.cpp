// programa logger
// yuzhi wu 51791
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
 #include <stdlib.h>
#include <cstring>
#define TAM 100

int main(int argc,char* argv[])
{
	
	const char * n_fifo = "/tmp/fifo";
	 if(mkfifo(n_fifo,0666)==-1)
	{		
		perror("error:mkfifo()");
		return 1;
	}
	int fd;
	fd=open(n_fifo,O_RDONLY);
	if (fd==-1)
	{ 
		perror("error:open()");
	   	exit(1);
	}
	char buf[TAM]="Comienza el juego\n";
	while(buf[0]!='F')
	{
		read (fd,&buf,TAM);
		write(1,&buf,strlen(buf)+1);
	}

  if(close (fd)<0)
	{
		 perror("error:close()");
	}
	unlink(n_fifo);

  return 0;
}

